Dado('que o cliente esteja na pagina de contratacao do {string}') do | product |
  @product = product
  @page.call(Home).load_product_url(@product)
  @page.call(Home).generate_product_data(@product)
end

Quando('preencher os dados de validacao iniciais') do
  @page.call(Home).fill_initial_data(@product)
  captcha_token = @service.call(CaptchaSolver).token_generator(@product)
  @page.call(Home).captcha_bypass(captcha_token)
end

Quando('preencher os dados pessoais') do
  @page.call(Home).fill_personal_data(@product)
end

Quando('as informacoes do certificado') do
  @page.call(Home).fill_certificate_data(@product)
end

Entao('devera visualizar o numero de seu pedido na tela de sucesso') do
  order_number = @page.call(Home).get_order_number
  @page.call(Home).save_order(@product,order_number)
end

Dado('que o agente de registro realize login no GAR') do
  @page.call(OrderApprove).load
  @page.call(OrderApprove).login
end

Quando('pesquisar pelo pedido do {string}') do |product|
  @product = product
  @page.call(AgentValidations).find_order(product)
end

Quando('preencher todo o cadastro') do
  @page.call(AgentValidations).edit_details
  @page.call(AgentValidations).click_radio_buttons
  @page.call(AgentValidations).representative_details
  @page.call(AgentValidations).holder_details
  @page.call(AgentValidations).intermediary_validations
  @page.call(AgentValidations).handle_dossier(@product)
end

Entao('devo visualizar a tela de sucesso na aprocavao') do
  @page.call(AgentValidations).verify_iti(@product)
end