class CaptchaSolver
  def token_generator(product)
    
    url = URI("http://api.dbcapi.me/api/captcha")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Post.new(url)
    form_data = [['authtoken', 'V1bH0uU0eJMOpQ5tWzCZuqbzHC44vt6HaFAq0C37BxXK6pW42NwX92XH8paSWIPx9Q8Eg2k11CERnMnl69MVD1WALcbEj29rjjJ9Xd0OddZibjT4e4dg01KulO0xHfEy6V5OnH3jrVv2L9NEe52XH4ZmBSEK'],['type', '4'],['token_params', '{"proxy":"","proxytype":"","googlekey":"6Lc194gUAAAAAIaCJocAtFo8oSjBKKo8yQaQFTzW","pageurl":"https://gestaoar-dev.certisign.com.br/GestaoAR/cliente/produto/inicio?produto=productHV2&grupo=DTEST&cliente_AR=DTEST&origem=1"}'.gsub("product",product)]]
    request.set_form form_data, 'multipart/form-data'
    response = http.request(request)
    captcha_id = response.body.split("=")[2].gsub("&text","")
    @token = 'xxx'
    60.times do
      begin
        request2 = Net::HTTP::Get.new(url + "/api/captcha/" + captcha_id)
        response2 = http.request(request2)
        token = response2.body.split("=")[3].gsub("&is_correct","")
        if token != ""
          @token = token
          break
        end
      rescue
        next
      end
      sleep 1
    end

    if @token == 'xxx'
    	raise 'token nao obtido'
    end

    return @token
  end
end