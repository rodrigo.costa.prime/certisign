# frozen_string_literal: true

Before do
  @page = lambda do |klass|
    klass.new
  end

  @service = lambda do |klass|
    klass.new
  end

  Capybara.reset_sessions!
end

at_exit do
  system "rake upload_report"
end

After do |scenario|
  screenshot = "./reports/screenshots/#{Time.now.strftime("%d%m%Y%H%M")}.png"
  page.save_screenshot screenshot
  attach screenshot , "image/png"
end