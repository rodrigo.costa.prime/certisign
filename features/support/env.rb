# frozen_string_literal: true

require 'capybara/dsl'
require 'cucumber'
require 'faker'
require 'pry'
require 'rspec'
require 'rspec/expectations'
require 'selenium/webdriver'
require 'site_prism'
require 'yaml'
require 'autoit'
require 'win32ole'

system "rm reports/cucumber.json"

World(Capybara::DSL)

PRODUCT_DATA_TEMPLATE = YAML.load_file("./features/fixtures/yml/product_data_template.yml")

Capybara.register_driver :selenium do |app|
    options = Selenium::WebDriver::Chrome::Options.new()
    options.add_extension("./features/support/certisign.crx")
   #options.add_argument('user-data-dir=C:\\Users\\Ronaldo.Romao\\AppData\\Local\\Google\\Chrome\\User Data')
    Capybara::Selenium::Driver.new(
	 	app, 
	 	browser: :chrome, 
	 	options: options)
    end
Capybara.current_driver = :selenium
Capybara.default_max_wait_time = 10