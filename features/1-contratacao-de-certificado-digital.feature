# language: pt
Funcionalidade: Contratacao de Certificado Digital

    Eu como cliente Certisign
    Gostaria de criar pedidos de Certificado Digital
    Para garantir minha autenticidade digital

    @AUTOMATED @cenario1 @POC-2 
    Esquema do Cenário: Criar Pedidos de Certificado Digital
            	Dado que o cliente esteja na pagina de contratacao do "<produto>"
        		Quando preencher os dados de validacao iniciais
        		E preencher os dados pessoais
        		E as informacoes do certificado
        		Entao devera visualizar o numero de seu pedido na tela de sucesso
        
        	Exemplos:
        
        	| produto |
        	| ACMA3PF |
  #          | ACMA3PJ |
  #          | ACMA1PF |
  #          | ACMA1PJ |
                

    @AUTOMATED @cenario2 @POC-3 
    Esquema do Cenário: Aprovar Pedidos de Certificado Digital
            	Dado que o agente de registro realize login no GAR
        		Quando pesquisar pelo pedido do "<produto>"
                E preencher todo o cadastro
                Entao devo visualizar a tela de sucesso na aprocavao
        
        	Exemplos:
        
        	| produto |
        	| ACMA3PF |
  #          | ACMA3PJ |
  #          | ACMA1PF |
  #          | ACMA1PJ |

