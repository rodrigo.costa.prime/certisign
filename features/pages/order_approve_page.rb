class OrderApprove < SitePrism::Page

  set_url  "https://gestaoar-dev.certisign.com.br/ARMultipla/adm/autenticacao/inicio"

  element  :agent_name, '#certisigner-certificate-list > tbody > tr > td:nth-child(2)'
  element  :authenticate_button, "#span-autenticar-on"

  def load_product_url(product)
    visit("https://gestaoar-dev.certisign.com.br/GestaoAR/cliente/produto/inicio?produto=#{product}HV2&grupo=DTEST&cliente_AR=DTEST&origem=1")
  end

  def login
    sleep 3
    3.times do
      begin
        first_pop_up.click
        break
      rescue
        next
      end
    end

    3.times do
      begin
        page.evaluate_script(IO.read('./features/js/remove_element.js'))
        break
      rescue
        next
      end
    end

    3.times do
      begin
        agent_name.click
        authenticate_button.click
        break
      rescue
        next
      end
    end
  end

end