class AgentValidations < SitePrism::Page

  element  :forward_button, '#moveForward'
  element  :ar_name_field, '#groupsSelector'
  element  :ar_name_option, '.ui-menu-item'
  element  :station_name_field, '#stationsSelector'
  element  :station_name_option, '.ui-menu-item'
  element  :certificate_requisitions_button, '.btnRequisicoes'
  element  :order_search_field, '#searchParamPedido'
  element  :order_search_button, '#searchRequests'

  element  :details_button,'.BtnVerDetalhes'
  element  :edit_data_button, '#btnEditData'
  element  :state_field, '#cliente\.uf'
  element  :city_field, '#cliente\.municipio'
  element  :id_number_field, '#cliente\.rg' 
  element  :id_number_emitter_field, '#cliente\.orgaoEmissorRg'
  element  :id_number_state_field, '#cliente\.ufRg'
  element  :save_button, '#btnSave'
  element  :close_button, '.ui-button-text'

  elements :presential_radio_button, 'input[name="tipoValidacao"]'
  elements :legal_representative_radio_button, 'input[name="representantePresente"]'
  elements :holder_radio_button, 'input[name="titularPresente"]'

  element  :representatives_button, "#botaoRepresentantes"
  element  :representatives_continue_button, "#continuarRepresentantes"
  element  :new_representative_button, "#legalRepresentatives > ul > li.GARBtnLaranjaMod"
  element  :id_representative_field, "#cpf"
  element  :save_representative_button, "#btnSaveLegalRepresentative"
  element  :representative_appearance, '#listaRepresentantes > tbody > tr.representativeRow > td:nth-child(4) > a > img'
  element  :detail_hair, '#selectedCharactId\[0\]'
  element  :detail_eyes, '#selectedCharactId\[1\]'
  element  :detail_etnie, '#selectedCharactId\[2\]'
  element  :save_appearance, '#charactFormSerialize > ul.GARBtnListIntMod2 > li.GARBtnLaranjaMod > a'
  element  :appearance_close, '#insertPhysicalCharactSuccessMessage1 > ul > li > a'
  element  :close_representative_pop_up, '#newLegalRepresentative > ul > li.GARBtnAzulMod > a'

  element  :holder_appearance, '#infoContainer > ul.GARBtnCinza > li:nth-child(5) > span'
  element  :save_holder_appearance, '#charactFormSerialize > ul.GARBtnListIntMod2 > li.GARBtnLaranjaMod > a'
  element  :close_holder_appearance, '#insertPhysicalCharactSuccessMessage1 > ul > li > a'

  element  :validation_button,  "#btnValidacao"
  element  :sign_term_1_button, '#GARBoxContent > ul.GARBtnCinza > li:nth-child(2) > span'
  element  :sign_term_2_button, '#formAssinaturaHash > ul > li > span'
  elements :signed_term_confirmation, '.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only'
  elements :dialog_buttons, '.ui-button-text'
  element  :confirm_button, '.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only'
  element  :dossier_button, '#btnGerarLote'
  element  :dossier_confirmation_button, '#confirmar'
  elements :confirm_button_2, '.btn.btn-success'

  element  :verify_iti_button, "#btnConsultarITI"
  element  :verify_ocurrences_button, "#btnConsultarOcorrencias"
  element  :no_results_button, "#semResultado"
  element  :validate_iti_button, "#btnValidate"
  element  :validate_frauds_button, "#btnValidateFrauds"
  elements :ar_emission_radio, "#issueType"
  element  :confirm_emission_button, "body > div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all > div.ui-dialog-buttonpane.ui-widget-content.ui-helper-clearfix > div > button:nth-child(2)"
  element  :sucess_pop_up, '#revalidateMessage'

  def find_order(product)
    @product_details = YAML.load_file("./features/fixtures/yml/#{product}.yml")
    sleep 3
    ar_name_field.set 'AR para Testes Automatizados - Grupo para Testes Automatizados'
    ar_name_option.click
    forward_button.click
    station_name_field.click
    station_name_field.send_keys 'Posto para Testes Automatizados'
    station_name_option.click
    forward_button.click
    certificate_requisitions_button.click
    order_search_field.set @product_details['PEDIDO']
    order_search_button.click
  end

  def edit_details
    sleep 5
    details_button.click
    edit_data_button.click
    state_field.select('SP')
    city_field.select('Sao Paulo')
    id_number_field.click
    id_number_field.send_keys '999999999'
    id_number_emitter_field.send_keys 'SSP'
    id_number_state_field.select('SP')
    save_button.click
    close_button.click
  end

  def click_radio_buttons
    presential_radio_button[2].click
    begin
      legal_representative_radio_button[0].click
    rescue
      sleep 1
    end
    holder_radio_button[1].click
  end

  def representative_details
    representatives_button.click
    representatives_continue_button.click
    new_representative_button.click
    id_representative_field.click  
    id_representative_field.send_keys '33006373847'
    id_representative_field.send_keys :tab
    sleep 1
    save_representative_button.click
    sleep 1
    page.driver.browser.switch_to.alert.accept
    begin
      if save_representative_button
        close_representative_pop_up.click
      end
    rescue
      sleep 1
    end
    representative_appearance.click
    detail_hair.select('Escuro')
    detail_eyes.select('Claros')
    detail_etnie.select('Indígena')
    save_appearance.click
    appearance_close.click
  end

  def holder_details
    holder_appearance.click
    sleep 1
    detail_hair.select('Escuro')
    sleep 1
    detail_eyes.select('Claros')
    sleep 1
    detail_etnie.select('Indígena')
    save_appearance.click
    sleep 1
    appearance_close.click
    sleep 1
    begin
      close_holder_appearance.click
    rescue
      sleep 1
    end
  end

  def intermediary_validations
    validation_button.click
    sleep 1
    wait_until_sign_term_1_button_visible
    sleep 3
    sign_term_1_button.double_click 
    sign_term_2_button.click 
    page.evaluate_script(IO.read('./features/js/remove_element.js'))
    signed_term_confirmation[1].send_keys :enter
    begin
      control = AutoIt::Control.new
      control.command('WinWaitActive', ['Logon do Token', nil , 30])
      control.command('ControlSetText', ['Logon do Token', '', 'Edit2','Cert@123'])
      sleep 1
      control.command('ControlClick', ['Logon do Token', '', 'Button1'])
    rescue
      p 'token A1'
    end
    signed_term_confirmation[0].send_keys :enter
  end

  def handle_dossier(product)
  	@product_details = YAML.load_file("./features/fixtures/yml/#{product}.yml")
    sleep 3
    dossier_button.click
    sleep 5
    windows = page.driver.browser.window_handles
    page.driver.switch_to_window(windows.last)
    sleep 5
    confirm_button_2[0].click
    sleep 5
    confirm_button_2[0].click
    sleep 5
    begin
      page.driver.browser.switch_to.alert.accept
    rescue
      sleep 1
    end
#    p '1'
    screenshot = "./reports/screenshots/1.png"
    page.save_screenshot screenshot
    control = AutoIt::Control.new
#    p '2'
    screenshot = "./reports/screenshots/2.png"
    page.save_screenshot screenshot
    control.command('WinActivate', ['Open', ''])
#    p '3'
    screenshot = "./reports/screenshots/3.png"
    page.save_screenshot screenshot
    control.command('WinWaitActive', ['Open', nil , 30])
#    p '4'
    screenshot = "./reports/screenshots/4.png"
    page.save_screenshot screenshot
    sleep 5
#    p '5'
    screenshot = "./reports/screenshots/5.png"
    page.save_screenshot screenshot
    control.command('ControlSetText', ['Open', '', 'Edit1','Certisign.jpg'])
#    p '6'
    screenshot = "./reports/screenshots/6.png"
    page.save_screenshot screenshot
    control.command('ControlClick', ['Open', '', 'Button1'])
#    p '7'
    screenshot = "./reports/screenshots/7.png"
    page.save_screenshot screenshot

#    control.command('WinActivate', ['Abrir', ''])
##    p '8'
#    screenshot = "./reports/screenshots/8.png"
#    page.save_screenshot screenshot
#    control.command('WinWaitActive', ['Abrir', nil , 30])
##    p '9'
#    screenshot = "./reports/screenshots/9.png"
#    page.save_screenshot screenshot
#    sleep 5
##    p '10'
#    screenshot = "./reports/screenshots/10.png"
#    page.save_screenshot screenshot
#    control.command('ControlSetText', ['Abrir', '', 'Edit1','Certisign.jpg'])
##    p '11'
#    screenshot = "./reports/screenshots/11.png"
#    page.save_screenshot screenshot
#    control.command('ControlClick', ['Abrir', '', 'Button1'])
 #   p '12'
 #   screenshot = "./reports/screenshots/12.png"
 #   page.save_screenshot screenshot

    sleep 1
#    p '13'
    screenshot = "./reports/screenshots/13.png"
    page.save_screenshot screenshot
    dossier_confirmation_button.click
#    p '14'
    screenshot = "./reports/screenshots/14.png"
    page.save_screenshot screenshot
    sleep 2
    confirm_button_2[0].click
    sleep 1
    3.times do
      begin
        click_button 'Fechar'
      rescue
        sleep 1
      end
    end

    sleep 1
    windows = page.driver.browser.window_handles
    sleep 1
    page.driver.switch_to_window(windows.last)
    sleep 1
    certificate_requisitions_button.click
    sleep 1
    order_search_field.set @product_details['PEDIDO']
    sleep 1
    order_search_button.click
    sleep 1
    details_button.click
  end

  def verify_iti(product)
  	@product_details = YAML.load_file("./features/fixtures/yml/#{product}.yml")
  	sleep 1
    validation_button.click
  	sleep 5
    verify_iti_button.click
  	sleep 1
    id_representative_field.select(@product_details['CPF'].to_i)
  	sleep 1
    verify_ocurrences_button.click
    sleep 3
    no_results_button.click
    sleep 3
    begin
      page.driver.browser.switch_to.alert.accept
    rescue
      sleep 1
    end
    id_representative_field.select(33006373847)
    verify_ocurrences_button.click
    sleep 3
    no_results_button.click
    sleep 3
    begin
      page.driver.browser.switch_to.alert.accept
    rescue
      sleep 1
    end
  	sleep 3
    validate_iti_button.click
  	sleep 3
    validate_frauds_button.click
  	sleep 3
    begin
    	ar_emission_radio[0].click
	  rescue
		  sleep 1
	  end
  	sleep 3

    validate_iti_button.click
  	sleep 3

    begin
      confirm_emission_button.double_click
      page.driver.browser.switch_to.alert.accept
    rescue
      sleep 1
    end
    
    begin
      page.driver.browser.switch_to.alert.accept
      control = AutoIt::Control.new
      control.command('WinWaitActive', ['Logon do Token', nil , 30])
      control.command('ControlSetText', ['Logon do Token', '', 'Edit2','Cert@123'])
      sleep 1
      control.command('ControlClick', ['Logon do Token', '', 'Button1'])
    rescue
      a = 1
    end

    3.times do
      begin
        page.driver.browser.switch_to.alert.accept
      rescue
        sleep 1
      end
    end
    sleep 1
    begin
      sucess_pop_up
      sleep 1
      page.driver.browser.switch_to.alert.accept
	  rescue
      sleep 1
    end
  end

end