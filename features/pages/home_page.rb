class Home < SitePrism::Page

  element  :cpf_field, '#cpf'
  element  :name_field, '#nome'
  element  :birthday_field, '#dtNascimento'
  element  :cellphone_field, '#celular'
  element  :telephone_field, '#telefone'
  element  :profession_field, '#profissao'
  element  :email_field, '#email'
  element  :email2_field, '#emailConfirmacao'

  element  :cnpj_field, '#cnpj'
  element  :company_name_field, '#razaoSocial'
  element  :tax_regime_dropdown, '#regimeTributario'
  element  :state_dropdown, '#uf'
  element  :city_dropdown, '#municipio'
  element  :role_field, '#cargoPF'

  element  :next_step_button_1, '#btn-submit-passo-1'
  element  :next_step_button_pf_2, '#btn-submit-pf-passo-2'
  element  :next_step_button_pj_2, '#btn-submit-pj-passo-2'
  element  :next_step_button_3, '#btn-submit-passo-3'
  element  :accept_1_checkbox, '#aceitaPoliticaColetaDados'
  elements :order_results, '.colorDados'

  def load_product_url(product)
    visit("https://gestaoar-dev.certisign.com.br/GestaoAR/cliente/produto/inicio?produto=#{product}HV2&grupo=DTEST&cliente_AR=DTEST&origem=1")
  end

  def generate_product_data(product)
    	PRODUCT_DATA_TEMPLATE[product]['CPF'] = Faker::IDNumber.brazilian_citizen_number
    	PRODUCT_DATA_TEMPLATE[product]['NOME'] = Faker::FunnyName.four_word_name
			PRODUCT_DATA_TEMPLATE[product]['DTNASCIMENTO'] = Faker::Date.birthday(min_age: 18, max_age: 65).strftime("%d/%m/%Y")
			PRODUCT_DATA_TEMPLATE[product]['CELULAR'] = Faker::Number.number(digits: 11)
			PRODUCT_DATA_TEMPLATE[product]['TELEFONE'] = Faker::Number.number(digits: 10)
			PRODUCT_DATA_TEMPLATE[product]['PROFISSAO'] = Faker::Job.position
			PRODUCT_DATA_TEMPLATE[product]['EMAIL'] = PRODUCT_DATA_TEMPLATE[product]['NOME'].gsub(" ","_").gsub(".","_").gsub("__","_").downcase + "@gmail.com"
    if product[5..6] == 'PJ'
    	PRODUCT_DATA_TEMPLATE[product]['CNPJ'] = Faker::Company.brazilian_company_number 
    	PRODUCT_DATA_TEMPLATE[product]['RAZAO_SOCIAL'] = Faker::Company.name
    	PRODUCT_DATA_TEMPLATE[product]['CARGO'] = Faker::Job.position
    end
  end

  def fill_initial_data(product)
    cpf_field.set PRODUCT_DATA_TEMPLATE[product]['CPF']
    if product[5..6] == 'PJ'
      email_field.set PRODUCT_DATA_TEMPLATE[product]['EMAIL']
      cnpj_field.set PRODUCT_DATA_TEMPLATE[product]['CNPJ']
    end
  end

  def captcha_bypass(token)  	
    recaptcha_response = find("#g-recaptcha-response", :visible => false)
    page.execute_script("arguments[0].value = '#{token}'", recaptcha_response)
    next_step_button_1.click
  end

  def fill_personal_data(product)
  	if product[5..6] == 'PJ'
			company_name_field.set PRODUCT_DATA_TEMPLATE[product]['RAZAO_SOCIAL']
			tax_regime_dropdown.select(["Lucro Real","Lucro Presumido","Simples Nacional", "Desconheço"].sample)
			telephone_field.set PRODUCT_DATA_TEMPLATE[product]['TELEFONE']
			state_dropdown.select('SP')
			city_dropdown.select('Sao Paulo')
			next_step_button_pj_2.click
		end

	  name_field.set PRODUCT_DATA_TEMPLATE[product]['NOME']
		birthday_field.set PRODUCT_DATA_TEMPLATE[product]['DTNASCIMENTO']
		cellphone_field.set PRODUCT_DATA_TEMPLATE[product]['CELULAR']
		telephone_field.set PRODUCT_DATA_TEMPLATE[product]['TELEFONE']
		profession_field.set PRODUCT_DATA_TEMPLATE[product]['PROFISSAO']
  	if product[5..6] == 'PJ'
			role_field.set PRODUCT_DATA_TEMPLATE[product]['CARGO']
		end
		next_step_button_pf_2.click

  end

  def fill_certificate_data(product)
    email_field.set PRODUCT_DATA_TEMPLATE[product]['EMAIL']
    email2_field.set PRODUCT_DATA_TEMPLATE[product]['EMAIL']
    accept_1_checkbox.click
    next_step_button_3.click
  end

  def get_order_number
  	return order_results[0].text
  end

  def save_order(product,order_number)
  	PRODUCT_DATA_TEMPLATE[product]['PEDIDO'] = order_number
  	File.write("./features/fixtures/yml/#{product}.yml", PRODUCT_DATA_TEMPLATE[product].to_yaml)
    page.driver.quit
  end

end
